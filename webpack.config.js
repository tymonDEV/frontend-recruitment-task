const { resolve } = require('path');

module.exports = {
    entry: {
        polyfills: 'babel-polyfill',
        app: './public/front/js/main.js'
    },

    output: {
        path: resolve(__dirname + '/public/dist/'),
        filename: '[name].js'
    },

    module: {
        rules: [{
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: ['transform-runtime'],
                        presets: ['es2015']
                    }
                }
            }
        ]
    }
}