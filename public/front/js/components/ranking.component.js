import Component from '../component.js';
const axios = require('axios');

class Ranking extends Component {
  constructor(selector) {
    super(selector);
    this.numbers = [];
  }

  init() {
    axios.get('http://localhost:3000/numbers')
      .then((response) => {
        response.data.data.sort((a,b) => {
          return b - a;
        });
        this.numbers = response.data.data.map((number) => {
          return {
            id: number
          };
        });
        this.render();
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export default Ranking;