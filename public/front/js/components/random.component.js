import Component from '../component.js';
const axios = require('axios');

class RandomComponent extends Component {
  constructor(extendSelector) {
    super(extendSelector);
    this.numbers = [];
  }

  init() {
    axios.get('http://localhost:3000/random-numbers')
      .then((response) => {
        this.numbers = response.data.data.map((number) => {
          return {
            id: number
          };
        });
        this.setTimeForRender();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setTimeForRender() {
    this.render();
    setTimeout(() => {
      this.render().innerHTML = '';
      this.init();
    }, 10000);
  }

}

export default RandomComponent;