class Component {
  constructor(selector) {
    this.selector = selector;
  }

  init() {
    console.log(`Component: ${this.selector} initialized`);
  }

  getDOMElement() {
    return document.querySelector(this.selector);
  }

  render() {
    console.log(`Component: ${this.selector} rendered`);
    const container = this.getDOMElement();
    this.numbers.forEach((number) => {
      const listElement = document.createElement('li');
      listElement.classList.add('list-group-item');
      listElement.innerHTML = number.id;
      container.appendChild(listElement);
    });
    return container;
  }
}

export default Component;