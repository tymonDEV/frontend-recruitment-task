import 'bootstrap/dist/css/bootstrap.min.css';
import Ranking from './components/ranking.component.js';
import RandomComponent from './components/random.component.js';

const ranking = new Ranking('#numbers-ranking');
const randomComponent = new RandomComponent('#numbers-random');
ranking.init();
randomComponent.init();

